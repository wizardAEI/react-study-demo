/*
 * @Descripttion: 
 * @Author: Wang Dejiang(aei)
 * @Date: 2021-11-08 20:31:50
 * @LastEditors: Wang Dejiang(aei)
 * @LastEditTime: 2021-11-10 20:27:25
 */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App name="abc"/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

