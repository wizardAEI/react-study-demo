/*
 * @Descripttion: 
 * @Author: Wang Dejiang(aei)
 * @Date: 2021-11-08 20:31:49
 * @LastEditors: Wang Dejiang(aei)
 * @LastEditTime: 2021-11-10 20:34:31
 */
import './App.css';
import React, {Component} from 'react'


class Mouse extends Component {
  // 鼠标位置state
  state = {
    x: 0,
    y: 0
  }

  handleMouseMove = e => {
    this.setState({
      x: e.clientX,
      y: e.clientY
    })
  }

  componentDidMount() {
    window.addEventListener('mousemove', this.handleMouseMove)
  }
  //注意需要在最后取消监听，以优化项目
  componnetWillUnmount() {
    window.removeEventListener('mousemove', this.handleMouseMove)
  }

  render() {
    return this.props.children(this.state)
  }
}

Mouse.defaultProps = {
  state: null
}

class App extends Component {
  render() {
    return (
      <div>
        <Mouse> 
        {(mouse) => {
          return <p>鼠标位置：{mouse.x} {mouse.y}</p>
        }}
        </Mouse>
      </div>
    );
  }
}


export default App 
